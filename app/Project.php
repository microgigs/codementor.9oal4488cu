<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

use Carbon\CarbonPeriod;

class Project extends Model
{
    /**
     * append to api/json responses
     */
    public $appends = [
        'use_this_end_date',
        'period',
        'route',
    ];

    /**
     * [$hidden description]
     * @var [type]
     */
    public $hidden = [
        'id',
        'remarks',
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    protected static function booted()
    {
        /**
         * (implicit) global scope: Not deleted
         */
        static::addGlobalScope('notDeleted', function(Builder $builder) {
            $builder->whereNull('deleted_at');
        });
    }

    /**
     * Provide the Laravel-managed route as an attribute
     *
     * $project->route
     */
    public function getRouteAttribute()
    {
        return route('projects.show', $this);
    }

    /**
     * Normalize *real* end date per evaluation of presence/absence of 'planned' / 'actual'
     */
    public function getUseThisEndDateAttribute()
    {
        return $this->actual_end_date
            ? $this->actual_end_date
            : $this->planned_end_date
        ;
    }

    public function getPeriodAttribute()
    {
        return CarbonPeriod::create(
            $this->start_date,
            $this->use_this_end_date
        );
    }

    /**
     * return projects which have at least 1 active day between
     * 'start_date' and either 'actual_end_date' (or) 'planned_end_date' (inclusive)
     * that falls within the given date range
     */
    public function scopeActiveFor($query, $dateRange)
    {
        $activeQuery = $query
            ->whereBetween('start_date', $dateRange)
            ->orWhereBetween('planned_end_date', $dateRange)
            ->orWhereBetween('actual_end_date', $dateRange)
        ;

        #dd ( $activeQuery->toSql() );
        return $activeQuery;
    }

    /**
     * local scope: Candidate
     */
    public function scopeForCandidate($query, $candidate_id)
    {
        return $query->when($candidate_id, function($query, $candidate_id) {
            return $query
                ->where('candidate_id', $candidate_id)
            ;
        });
    }
}
