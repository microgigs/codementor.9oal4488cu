<?php

namespace App\Http\Controllers;

use App\Project;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $activeProjects = [];

        $dateRange = [
            'start' => new Carbon($request->start),
            'end' => new Carbon($request->end),
        ];

        $projects = Project::activeFor( $dateRange )
            //->forCandidate( $request->candidate )
            ->get()
        ;

        $queryPeriod = CarbonPeriod::create(
            $dateRange['start'],
            $dateRange['end']
        );

        // iterate each day of the queried period:
        foreach ( $queryPeriod as $key => $carbonDate)  {
            $onThisDate = $carbonDate->toDateString();
            $activeProjects[$onThisDate] = 0;

            // iterate detected active projects during this period
            foreach ( $projects as $project ) {
                if ( in_array($carbonDate, $project->period->toArray()) ) {
                    $activeProjects[$onThisDate]++;
                }
            }
        }

        //// as Laravel Collection operations:
        // $collected = collect($activeProjects);
        // $chartData = $collected->values()->toJson();
        // $chartLabels = $collected->keys()->toJson();

        //// as basic PHP array operations:
        $chartData = json_encode(array_values($activeProjects));
        $chartLabels = json_encode(array_keys($activeProjects));

        return view('analytics.projects',
            compact('chartData', 'chartLabels')
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return $project;
    }
}
