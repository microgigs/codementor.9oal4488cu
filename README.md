<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## A small Laravel + Chart.js example
`ProjectController@index` prepares a period of dates along with a chartable data set to be rendered out by Chart.js in the `analytics.projects` Blade view.
