@extends( '_layouts_.app' )

@section( 'content' )

  <h2>Project Analytics...</h2>

  <canvas id="projectChart" width="400" height="400"></canvas>

  <script defer>
    const ctx = document.getElementById('projectChart').getContext('2d')

    const projectChart = new Chart(ctx, {
      type: 'line',

      data: {
        labels: {!! $chartLabels !!},
        datasets: [
          {
            label: 'Active Projects per Day',
            data: {!! $chartData !!},
            borderWidth: 1,
          },
        ],
      },

      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                stepSize: 1,
              },
            },
          ],
          xAxes: [
            {
                type: 'category',
                //labels: {!! $chartLabels !!},
                ticks: {
                    maxTicksLimit: 12
                }
            }
          ],
        },
      },
    })
  </script>
@endsection